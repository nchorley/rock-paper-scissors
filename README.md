Java 8 is required, as I used the java.time package to seed the random number
generator to get a symbol for the computer player.

Building the application is done with Maven:

$ mvn package

will create a JAR in target/.

The JAR is executable so that the application can be run with

$ java -jar target/rock-paper-scissors-1.0-SNAPSHOT.jar

To measure code coverage, I have used the Maven Cobertura plugin. An HTML report
can be generated with

$ mvn cobertura:cobertura

and the index page of the report can be found at target/site/cobertura/index.html.
