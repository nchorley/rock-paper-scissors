package com.ebay.inputs;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class ModeInputConverterTest {
    @Test
    public void testHumanVsComputerMode() {
        Mode mode = ModeInputConverter.convert("1");

        assertThat(mode, equalTo(Mode.HUMAN_VS_COMPUTER));
    }

    @Test
    public void testComputerVsComputerMode() {
        Mode mode = ModeInputConverter.convert("2");

        assertThat(mode, equalTo(Mode.COMPUTER_VS_COMPUTER));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonNumericInputRejected() {
        ModeInputConverter.convert("human");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroInputRejected() {
        ModeInputConverter.convert("0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeInputsRejected() {
        ModeInputConverter.convert("-10");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInputGreaterThanNumberOfModesRejected() {
        ModeInputConverter.convert("3");
    }
}
