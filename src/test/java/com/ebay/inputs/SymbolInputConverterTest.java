package com.ebay.inputs;

import com.ebay.symbols.Symbol;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class SymbolInputConverterTest {
    @Test
    public void testRockInput() {
        Symbol symbol = SymbolInputConverter.convert("1");

        assertThat(symbol, equalTo(Symbol.ROCK));
    }

    @Test
    public void testPaperInput() {
        Symbol symbol = SymbolInputConverter.convert("2");

        assertThat(symbol, equalTo(Symbol.PAPER));
    }

    @Test
    public void testScissorsInput() {
        Symbol symbol = SymbolInputConverter.convert("3");

        assertThat(symbol, equalTo(Symbol.SCISSORS));
    }

    @Test
    public void testLizardInput() {
        Symbol symbol = SymbolInputConverter.convert("4");

        assertThat(symbol, equalTo(Symbol.LIZARD));
    }

    @Test
    public void testSpockInput() {
        Symbol symbol = SymbolInputConverter.convert("5");

        assertThat(symbol, equalTo(Symbol.SPOCK));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonNumericInputRejected() {
        SymbolInputConverter.convert("rock");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroInputRejected() {
        SymbolInputConverter.convert("0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeInputRejected() {
        SymbolInputConverter.convert("-1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInputGreaterThanNumberOfSymbolsRejected() {
        SymbolInputConverter.convert("6");
    }
}
