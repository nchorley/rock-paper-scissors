package com.ebay;


import com.ebay.generators.StaticSymbolGenerator;
import com.ebay.players.Player;
import com.ebay.players.PlayerFactory;
import com.ebay.symbols.Symbol;
import com.ebay.validators.AcceptAllNamesValidator;
import com.ebay.view.BasicViewFactory;
import com.ebay.view.View;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GameControllerTest {
    @Test
    public void testPlayer1Wins() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.LIZARD);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SPOCK);

        View view = gameController.play(player1, player2);

        assertThat(view.render(), equalTo("Lizard beats Spock, Nicky wins!"));
    }

    @Test
    public void testPlayer2Wins() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.ROCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.PAPER);

        View view = gameController.play(player1, player2);

        assertThat(view.render(), equalTo("Paper beats Rock, Kitcat wins!"));
    }

    @Test
    public void testDraw() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.SCISSORS);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SCISSORS);

        View view = gameController.play(player1, player2);

        assertThat(view.render(), equalTo("It's a draw!"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPlayRejectsPlayer1IfNull() {
        Player player = playerFactory.getComputerPlayer("Isles");
        gameController.play(null, player);
    }

   @Test(expected = IllegalArgumentException.class)
    public void testPlayRejectsPlayer2IfNull() {
       Player player = playerFactory.getComputerPlayer("Garcia");
        gameController.play(player, null);
    }

    private PlayerFactory playerFactory = new PlayerFactory(new AcceptAllNamesValidator(),
            new StaticSymbolGenerator(Symbol.PAPER));

    private GameController gameController = new GameController(new BasicViewFactory());

}
