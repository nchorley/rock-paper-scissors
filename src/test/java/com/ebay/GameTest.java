package com.ebay;

import com.ebay.generators.StaticSymbolGenerator;
import com.ebay.players.Player;
import com.ebay.players.PlayerFactory;
import com.ebay.symbols.Symbol;
import com.ebay.validators.AcceptAllNamesValidator;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GameTest {
    @Test
    public void testScissorsDrawsScissors() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.SCISSORS);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SCISSORS);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.DRAW));
    }

    @Test
    public void testScissorsBeatsPaper() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.PAPER);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SCISSORS);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER2_WIN));
    }

    @Test
     public void testRockBeatsScissors() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.ROCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SCISSORS);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER1_WIN));
    }

    @Test
     public void testPaperBeatsRock() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.ROCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.PAPER);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER2_WIN));
    }

   @Test
     public void testLizardBeatsPaper() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.LIZARD);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.PAPER);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER1_WIN));
    }

    @Test
     public void testRockBeatsLizard() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.LIZARD);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.ROCK);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER2_WIN));
    }

    @Test
     public void testScissorsBeatsLizard() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.LIZARD);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SCISSORS);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER2_WIN));
    }

    @Test
     public void testSpockBeatsScissors() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.SPOCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.SCISSORS);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER1_WIN));
    }

    @Test
     public void testPaperBeatsSpock() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.SPOCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.PAPER);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER2_WIN));
    }

    @Test
     public void testSpockBeatsRock() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.SPOCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.ROCK);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER1_WIN));
    }

    @Test
     public void testLizardBeatsSpock() {
        Player player1 = playerFactory.getHumanPlayer("Nicky", Symbol.SPOCK);
        Player player2 = playerFactory.getHumanPlayer("Kitcat", Symbol.LIZARD);
        Game game = new Game(player1, player2);

        Result result = game.getResult();

        assertThat(result, equalTo(Result.PLAYER2_WIN));
    }

    private PlayerFactory playerFactory = new PlayerFactory(new AcceptAllNamesValidator(),
            new StaticSymbolGenerator(Symbol.ROCK));
}
