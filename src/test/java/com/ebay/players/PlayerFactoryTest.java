package com.ebay.players;

import com.ebay.generators.StaticSymbolGenerator;
import com.ebay.symbols.Symbol;
import com.ebay.generators.SymbolGenerator;
import com.ebay.validators.AcceptAllNamesValidator;
import com.ebay.validators.RejectAllNamesValidator;
import com.ebay.validators.NameValidator;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;

public class PlayerFactoryTest {
    @Test(expected = IllegalArgumentException.class)
    public void testHumanPlayerWithBadNameNotCreated() {
        PlayerFactory factory = new PlayerFactory(rejectAllNamesValidator, symbolGenerator);

        factory.getHumanPlayer("Nicky", Symbol.ROCK);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHumanPlayerWithNoSymbolNotCreated() {
        PlayerFactory factory = new PlayerFactory(acceptAllNamesValidator, symbolGenerator);

        factory.getHumanPlayer("Nicky", null);
    }

    @Test
    public void testCreateHumanPlayer() {
        PlayerFactory factory = new PlayerFactory(acceptAllNamesValidator, symbolGenerator);
        String name = "Nicky";
        Symbol symbol = Symbol.SCISSORS;

        Player player = factory.getHumanPlayer(name, symbol);

        assertPlayerCreated(player, name, symbol);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testComputerPlayerWithBadNameNotCreated() {
        PlayerFactory factory = new PlayerFactory(rejectAllNamesValidator, symbolGenerator);

        factory.getComputerPlayer("Computer");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testComputerPlayerWithNoSymbolNotCreated() {
        SymbolGenerator nullSymbolGenerator = new StaticSymbolGenerator(null);
        PlayerFactory factory = new PlayerFactory(acceptAllNamesValidator, nullSymbolGenerator);

        factory.getComputerPlayer("Computer");
    }

    @Test
    public void testCreateComputerPlayer() {
        PlayerFactory factory = new PlayerFactory(acceptAllNamesValidator, symbolGenerator);
        String name = "Computer";

        Player player = factory.getComputerPlayer(name);

        assertPlayerCreated(player, name, Symbol.PAPER);
    }

    private NameValidator acceptAllNamesValidator = new AcceptAllNamesValidator();
    private NameValidator rejectAllNamesValidator = new RejectAllNamesValidator();
    private SymbolGenerator symbolGenerator = new StaticSymbolGenerator(Symbol.PAPER);

    private static void assertPlayerCreated(Player player, String name, Symbol symbol) {
        assertThat(player, notNullValue());
        assertThat(player.getName(), equalTo(name));
        assertThat(player.getSymbol(), equalTo(symbol));
    }
}
