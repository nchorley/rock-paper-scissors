package com.ebay.players;

import com.ebay.symbols.Symbol;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;

public class PlayerTest {
    @Test
    public void testPlayerEqualToItself() {
        Player player = new Player("Nicky", Symbol.SPOCK);

        assertThat(player, equalTo(player));
    }

    @Test
    public void testDifferentPlayerEquality() {
        String name = "Nicky";
        Symbol symbol = Symbol.LIZARD;

        Player player1 = new Player(name, symbol);
        Player player2 = new Player(name, symbol);

        assertThat(player1, equalTo(player2));
    }

    @Test
    public void testPlayersWithDifferentNamesAreNotEqual() {
        Player player1 = new Player("Nicky", Symbol.PAPER);
        Player player2 = new Player("Kitcat", Symbol.PAPER);

        assertThat(player1, not(equalTo(player2)));
    }

    @Test
    public void testPlayersWithDifferentSymbolsAreNotEqual() {
        Player player1 = new Player("Nicky", Symbol.ROCK);
        Player player2 = new Player("Nicky", Symbol.SCISSORS);

        assertThat(player2, not(equalTo(player1)));
    }

    @Test
    public void testPlayerNotEqualToNull() {
        Player player = new Player("Nicky", Symbol.LIZARD);

        assertThat(player, not(equalTo(null)));
    }

    @Test
    public void testPlayerNotEqualToObjectOfAnotherClass() {
        Player player = new Player("Kitcat", Symbol.ROCK);
        String string = "Kitcat";

        assertThat(player.equals(string), is(false));
    }
}
