package com.ebay.generators;

import com.ebay.symbols.Symbol;

public class StaticSymbolGenerator implements SymbolGenerator {
    public StaticSymbolGenerator(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    private Symbol symbol;
}
