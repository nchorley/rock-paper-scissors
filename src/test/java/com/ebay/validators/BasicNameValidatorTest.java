package com.ebay.validators;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class BasicNameValidatorTest {
    @Test
    public void testNullNameIsInvalid() {
        boolean result = validator.isValid(null);

        assertThat(result, equalTo(false));
    }

    @Test
    public void testEmptyNameIsInvalid() {
        boolean result = validator.isValid("");

        assertThat(result, equalTo(false));
    }

    @Test
    public void testOtherNamesAreValid() {
        boolean result = validator.isValid("Nicky");

        assertThat(result, equalTo(true));
    }

    private NameValidator validator = new BasicNameValidator();
}
