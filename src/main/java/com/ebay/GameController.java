package com.ebay;

import com.ebay.players.Player;
import com.ebay.view.View;
import com.ebay.view.ViewContext;
import com.ebay.view.ViewFactory;

public class GameController {
    public GameController(ViewFactory viewFactory) {
        this.viewFactory = viewFactory;
    }

    public View play(Player player1, Player player2) {
        if (player1 == null || player2 == null) {
            throw new IllegalArgumentException();
        }

        Game game = new Game(player1, player2);
        Result result = game.getResult();

        View view = viewFactory.getView();
        view.setContext(createViewContext(player1, player2, result));

        return view;
    }

    private ViewContext createViewContext(Player player1, Player player2, Result result) {
        Player winner = null;
        Player loser = null;

        if (result.equals(Result.PLAYER1_WIN)) {
            winner = player1;
            loser = player2;
        } else if (result.equals(Result.PLAYER2_WIN)) {
            winner = player2;
            loser = player1;
        }

        return new ViewContext(winner, loser, result);
    }

    private ViewFactory viewFactory;
}
