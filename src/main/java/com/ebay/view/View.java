package com.ebay.view;

public interface View {
    /**
     * Set the view context to be used when rendering this view.
     *
     * @param viewContext the game information to be rendered.
     * @throws IllegalArgumentException if viewContext is null.
     */
    public void setContext(ViewContext viewContext);

    /**
     * Render this view.
     *
     * @return The string representation of this view, which should not be null.
     */
    public String render();
}
