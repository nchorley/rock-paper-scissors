package com.ebay.view;

public class BasicViewFactory implements ViewFactory {
    public View getView() {
        return new BasicView();
    }
}
