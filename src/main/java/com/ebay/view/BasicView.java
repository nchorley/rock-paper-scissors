package com.ebay.view;

import com.ebay.Result;
import com.ebay.players.Player;

public class BasicView implements View {
    public void setContext(ViewContext viewContext) {
        this.viewContext = viewContext;
    }

    public String render() {
        Player winner = viewContext.getWinner();
        Player loser = viewContext.getLoser();

        String message;

        if (viewContext.getResult().equals(Result.DRAW)) {
            message = "It's a draw!";
        } else {
            message = getMessage(winner, loser);
        }

        return message;
    }

    private static String getMessage(Player winner, Player loser) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(winner.getSymbol());
        stringBuilder.append(" beats ");
        stringBuilder.append(loser.getSymbol());
        stringBuilder.append(", ");
        stringBuilder.append(winner.getName());
        stringBuilder.append(" wins!");

        return stringBuilder.toString();
    }

    private ViewContext viewContext;
}
