package com.ebay.view;

public interface ViewFactory {
    /**
     * Get a view to be displayed.
     *
     * @throws IllegalStateException if a View cannot be created.
     * @return The View to display. This should not be null.
     */
    public View getView();
}
