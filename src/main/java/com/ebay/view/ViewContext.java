package com.ebay.view;

import com.ebay.Result;
import com.ebay.players.Player;

public class ViewContext {
    public ViewContext(Player winner, Player loser, Result result) {
        this.winner = winner;
        this.loser = loser;
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    public Player getWinner() {
        return winner;
    }

    public Player getLoser() {
        return loser;
    }

    private Player winner;
    private Player loser;
    private Result result;
}
