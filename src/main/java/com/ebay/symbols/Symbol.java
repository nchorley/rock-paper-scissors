package com.ebay.symbols;

import java.util.HashSet;
import java.util.Set;

public enum Symbol {
    ROCK,
    PAPER,
    SCISSORS,
    LIZARD,
    SPOCK;

    public static void setSuperiorTo() {
        SCISSORS.superiorTo.add(PAPER);
        ROCK.superiorTo.add(SCISSORS);
        PAPER.superiorTo.add(ROCK);
        LIZARD.superiorTo.add(PAPER);
        ROCK.superiorTo.add(LIZARD);
        SCISSORS.superiorTo.add(LIZARD);
        SPOCK.superiorTo.add(SCISSORS);
        PAPER.superiorTo.add(SPOCK);
        SPOCK.superiorTo.add(ROCK);
        LIZARD.superiorTo.add(SPOCK);
    }

    public boolean beats(Symbol symbol) {
        return this.superiorTo.contains(symbol);
    }

    public String toString() {
        String original = this.name();
        return original.substring(0, 1) + original.substring(1).toLowerCase();
    }

    private final Set<Symbol> superiorTo = new HashSet<Symbol>();
}
