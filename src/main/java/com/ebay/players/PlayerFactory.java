package com.ebay.players;

import com.ebay.symbols.Symbol;
import com.ebay.generators.SymbolGenerator;
import com.ebay.validators.NameValidator;

public class PlayerFactory {
    public PlayerFactory(NameValidator nameValidator, SymbolGenerator symbolGenerator) {
        this.nameValidator = nameValidator;
        this.symbolGenerator = symbolGenerator;
    }

    public Player getHumanPlayer(String name, Symbol symbol) {
        return getPlayer(name, symbol);
    }

    public Player getComputerPlayer(String name) {
        return getPlayer(name, symbolGenerator.getSymbol());
    }

    private Player getPlayer(String name, Symbol symbol) {
        if (!nameValidator.isValid(name) || symbol == null) {
            throw new IllegalArgumentException();
        }

        return new Player(name, symbol);
    }

    private NameValidator nameValidator;
    private SymbolGenerator symbolGenerator;
}
