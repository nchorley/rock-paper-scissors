package com.ebay.players;

import com.ebay.symbols.Symbol;

public class Player {
    Player(String name, Symbol symbol) {
        this.name = name;
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public boolean equals(Object object) {
        if (object == null || !getClass().equals(object.getClass())) {
            return false;
        }

        Player otherPlayer = (Player)object;

        return name.equals(otherPlayer.name) && symbol.equals(otherPlayer.symbol);
    }

    private String name;
    private Symbol symbol;
}
