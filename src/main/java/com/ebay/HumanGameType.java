package com.ebay;

import java.util.Scanner;

import com.ebay.generators.NameGenerator;
import com.ebay.inputs.SymbolInputConverter;
import com.ebay.players.Player;
import com.ebay.players.PlayerFactory;
import com.ebay.symbols.Symbol;

public class HumanGameType extends GameType {
    public HumanGameType(PlayerFactory playerFactory, NameGenerator nameGenerator) {
        super(playerFactory, nameGenerator);
        this.scanner = new Scanner(System.in);
    }

    @Override
    protected void getPlayers() {
        player1 = playerFactory.getComputerPlayer(nameGenerator.getName());
        player2 = getHumanPlayer();
    }

    private Player getHumanPlayer() {
        boolean inputIsValid = true;

        Player player = null;

        do {
            try {
                String name = getName();
                System.out.println();
                Symbol symbol = getSymbol();

                player = playerFactory.getHumanPlayer(name, symbol);
                inputIsValid = true;
            } catch (IllegalArgumentException e) {
                inputIsValid = false;
                System.out.println("Your input was invalid, try again");
                System.out.println();
            }

        } while (!inputIsValid);

        return player;
    }

    private Scanner scanner;

    private String getName() {
        System.out.print("Enter your name: ");
        return scanner.nextLine();
    }

    private Symbol getSymbol() {
        displaySymbols();
        System.out.println("Choose a symbol: ");

        String input = scanner.nextLine();

        return SymbolInputConverter.convert(input);
    }

    private void displaySymbols() {
        for (Symbol s : Symbol.class.getEnumConstants()) {
            System.out.println((s.ordinal() + 1) + ". " + s.toString());
        }

        System.out.println();
    }
}
