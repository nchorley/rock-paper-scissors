package com.ebay;

public enum Result {
    DRAW,
    PLAYER1_WIN,
    PLAYER2_WIN,
}
