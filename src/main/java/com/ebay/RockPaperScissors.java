package com.ebay;

import java.util.Scanner;

import com.ebay.generators.BasicSymbolGenerator;
import com.ebay.generators.NameGenerator;
import com.ebay.inputs.Mode;
import com.ebay.inputs.ModeInputConverter;
import com.ebay.players.PlayerFactory;
import com.ebay.validators.BasicNameValidator;


public class RockPaperScissors {
    public static void main(String[] args) {
        PlayerFactory playerFactory = new PlayerFactory(new BasicNameValidator(),
                new BasicSymbolGenerator());

        String[] computerPlayerNames = {"Garcia", "Isles", "Sciuto", "Morgan", "Gibbs"};
        NameGenerator nameGenerator = new NameGenerator(computerPlayerNames);

        System.out.println("Play rock, paper, scissors!");
        System.out.println();

        boolean playAgain = true;

        do {
            Mode mode = getMode();

            GameType gameType = null;

            if (mode.equals(Mode.COMPUTER_VS_COMPUTER)) {
                gameType = new ComputerGameType(playerFactory, nameGenerator);
            } else if (mode.equals(Mode.HUMAN_VS_COMPUTER)) {
                gameType = new HumanGameType(playerFactory, nameGenerator);
            }

            gameType.play();

            String input = getUserInput("Play again (Y/N)? ");
            playAgain = input.equalsIgnoreCase("Y");
        } while (playAgain);
    }

     public static Mode getMode() {
         displayModes();

         boolean inputIsValid = true;
         Mode mode = null;

         do {
            String input = getUserInput("Enter your choice: ");

            try {
                mode = ModeInputConverter.convert(input);
                inputIsValid = true;
            } catch (IllegalArgumentException e) {
                inputIsValid = false;
                System.out.println("Your input was invalid, try again");
                System.out.println();
            }
        } while (!inputIsValid);

         return mode;
    }

    public static String getUserInput(String prompt) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(prompt);

        return scanner.nextLine();
    }


    public static void displayModes() {
        System.out.println("1. Human vs. Computer");
        System.out.println("2. Computer vs. Computer");
        System.out.println();
    }
}
