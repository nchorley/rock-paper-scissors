package com.ebay;

import com.ebay.players.Player;
import com.ebay.symbols.Symbol;

public class Game {
    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;

        Symbol.setSuperiorTo();
    }

    Result getResult() {
        Symbol symbol1 = player1.getSymbol();
        Symbol symbol2 = player2.getSymbol();

        Result result = Result.DRAW;

        if (symbol1.beats(symbol2)) {
            result = Result.PLAYER1_WIN;
        } else if (symbol2.beats(symbol1)) {
            result = Result.PLAYER2_WIN;
        }

        return result;
    }

    private Player player1;
    private Player player2;
}
