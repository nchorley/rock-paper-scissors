package com.ebay.inputs;

public enum Mode {
    HUMAN_VS_COMPUTER,
    COMPUTER_VS_COMPUTER
}
