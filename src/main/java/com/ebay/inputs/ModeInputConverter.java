package com.ebay.inputs;

import com.ebay.inputs.Mode;

public class ModeInputConverter {
    public static Mode convert(String input) {
        Mode[] modes = Mode.class.getEnumConstants();
        int index = Integer.parseInt(input) - 1;

        if (index < 0 || index >= modes.length) {
            throw new IllegalArgumentException();
        }

        return modes[index];
    }
}
