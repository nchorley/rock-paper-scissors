package com.ebay.inputs;

import com.ebay.symbols.Symbol;

public class SymbolInputConverter {
    public static Symbol convert(String input) {
        Symbol[] symbols = Symbol.class.getEnumConstants();
        int index = Integer.parseInt(input) - 1;

        if (index < 0 || index >= symbols.length) {
            throw new IllegalArgumentException();
        }

        return symbols[index];
    }
}
