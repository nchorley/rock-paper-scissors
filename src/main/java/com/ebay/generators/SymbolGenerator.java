package com.ebay.generators;

import com.ebay.symbols.Symbol;

public interface SymbolGenerator {
    /**
     * Returns a random Symbol instance.
     *
     * This method should not return null.
     *
     * @return a Symbol
     */
    public Symbol getSymbol();
}
