package com.ebay.generators;

import java.time.Instant;
import java.util.Random;

public class NameGenerator {
    public NameGenerator(String[] names) {
        this.names = names;
    }

    public String getName() {
        Instant now = Instant.now();
        Random random = new Random(now.toEpochMilli());
        int index = random.nextInt(names.length);

        return names[index];
    }

    private static String[] names;
}
