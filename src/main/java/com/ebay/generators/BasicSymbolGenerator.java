package com.ebay.generators;

import java.util.Random;
import java.time.Instant;

import com.ebay.symbols.Symbol;

public class BasicSymbolGenerator implements SymbolGenerator {
    public Symbol getSymbol() {
        Instant now = Instant.now();
        Random random = new Random(now.toEpochMilli());
        int index = random.nextInt(symbols.length);

        return symbols[index];
    }

    private Symbol[] symbols = Symbol.class.getEnumConstants();
}
