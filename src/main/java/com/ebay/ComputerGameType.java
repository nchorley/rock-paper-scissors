package com.ebay;

import com.ebay.generators.NameGenerator;
import com.ebay.players.Player;
import com.ebay.players.PlayerFactory;

public class ComputerGameType extends GameType {
    public ComputerGameType(PlayerFactory playerFactory, NameGenerator nameGenerator) {
        super(playerFactory, nameGenerator);
    }

    @Override
    protected void getPlayers() {
        player1 = playerFactory.getComputerPlayer(nameGenerator.getName());
        player2 = playerFactory.getComputerPlayer(nameGenerator.getName());
    }
}
