package com.ebay.validators;

public class BasicNameValidator implements NameValidator {
    public boolean isValid(String name) {
        boolean result = true;

        if (name == null || name.isEmpty()) {
            result = false;
        }

        return result;
    }
}
