package com.ebay.validators;

public interface NameValidator {
    /**
     * Checks whether the name is valid, according to some rules.
     *
     * This method should return false if the name is null or empty.
     *
     * @param name   the name to be validated
     * @return       true if the name is valid, false otherwise
     */
    public boolean isValid(String name);
}
