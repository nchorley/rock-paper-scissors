package com.ebay;

import com.ebay.generators.NameGenerator;
import com.ebay.players.Player;
import com.ebay.players.PlayerFactory;
import com.ebay.view.BasicViewFactory;
import com.ebay.view.View;
import com.ebay.view.ViewFactory;

public abstract class GameType {
    public GameType(PlayerFactory playerFactory, NameGenerator nameGenerator) {
        this.playerFactory = playerFactory;
        this.nameGenerator = nameGenerator;
    }

    public void play() {
        getPlayers();
        displayPlayerInfo();

        ViewFactory viewFactory = new BasicViewFactory();
        GameController gameController = new GameController(viewFactory);

        View view = gameController.play(player1, player2);

        display(view);
    }

    protected abstract void getPlayers();

    protected void displayPlayerInfo() {
        System.out.println(getPlayerInfo(player1));
        System.out.println(getPlayerInfo(player2));
        System.out.println();
    }

    protected String getPlayerInfo(Player player) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Name: ");
        stringBuilder.append(player.getName());
        stringBuilder.append(", symbol: ");
        stringBuilder.append(player.getSymbol());

        return stringBuilder.toString();
    }

    protected void display(View view) {
        System.out.println(view.render());
        System.out.println();
    }

    protected PlayerFactory playerFactory;
    protected NameGenerator nameGenerator;
    protected Player player1;
    protected Player player2;
}
