2015-12-06

It seems like an MVC approach is reasonable here, as I'm imagining
wanting to make calls to a controller like play() and that uses the
model (Game) to get the result and generate something that can be
displayed. The request/response model just kinda "works" in my brain.

I thus chose to have the ViewFactory and View interfaces, so that
different clients can specify how to display the information. render()
just returns a String, because a View just represents something to
be displayed. Control over where that gets displayed (e.g. standard out,
a file, sent back to a client) should belong somewhere else.

Also, the Game is really the only thing that cares about the
relationships between the symbols; the controller does not need to
know about that. I thus chose to refactor so that the Game calls
Symbol.setSuperiorTo().

The logic that checks whether players are null or not can now lie in
the controller, so that it validates the inputs before trying to play
a game. I'm not too sure whether it's better to throw
IllegalArgumentException, or return a View that has details of errors
in it, so will go with the former for now.
