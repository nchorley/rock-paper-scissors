2015-12-02

I now want to proceed with creating something that will model the Game and want
to test the happy path first. That is, when we have a game with two players, I
can ask who the winner is.

For this, I need to instantiate the factory, which has dependencies on a
NameValidator and SymbolGenerator. I already had code that mocked these and now
decided to replace the mocks with implementations that did the same thing. This
prevents me from having one "Helper" class or something with a load of unrelated
static methods. Single responsibility in the tests, too!

It seems like the most natural interface is to have a getWinner() method that
returns the Player who won. To test this, I now need to implement equality. Right
now, I have not overridden hashCode() as again, I do not yet need to store players
in a hash table.

2015-12-03

There are problems with the above. Firstly, we're at risk of basically duplicating the
stuff in SymbolComparator, i.e. with code like

Result result = symbolComparator.compare(player1.getSymbol(), player2.getSymbol());

Player winner;

if (result.equals(Result.DRAW)) {
    winner = ? // Possibly some "null" player
} else if (result.equals(Result.LEFT_WIN)) {
    winner = player1;
} else if (result.equals(Result.RIGHT_WIN)) {
    winner = player2;
}

So, I guess the SymbolComparator class now becomes unnecessary and the logic can be
put in the Game class directly.

Another problem is how to deal with a draw? If we return a Player, then, as above,
I guess that implies using the null object pattern. It seems like the null object
there would be a Player with an empty String (or null) for the name and null for
the symbol. The code making use of that object would then have possibilities of
throwing NullPointerExceptions and having to do something like

if (winner.getName().isEmpty()) {
    // Do the appropriate thing for a draw
}

is not a clear expression of intent (as in, the condition being checked doesn't
read like, "the game was a draw") and also looks like a violation of the law of
Demeter.

So, I think that Game should just have a getResult() method that returns the
appropriate Result enumeration.
